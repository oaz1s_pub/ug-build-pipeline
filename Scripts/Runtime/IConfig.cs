﻿using System;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

namespace OAZ1S.BuildPipeline
{
    public class IConfig<T> : ScriptableObject where T : IConfig<T>
    {
        protected static string PREFAB_NAME = "";
        protected static string FILE_PATH = "";

#region Instance

        [NonSerialized] private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    if (Application.isPlaying)
                    {
                        var so = ScriptableObject.CreateInstance<T>();
                        var asset = GetByPath(Path.Combine(FILE_PATH, PREFAB_NAME));
                        if (asset != null)
                        {
                            _instance = Instantiate(asset);
                        }

                        Destroy(so);
                    }
                    else
                    {
#if UNITY_EDITOR

                        return GetConfigFile();

#endif // UNITY_EDITOR
                    }
                }

                return _instance;
            }
        }

        private static T GetByPath(string prefabPath)
        {
            return Resources.Load<T>(prefabPath);
        }

#endregion Instance
        
        #if UNITY_EDITOR
        
        protected static void SelectConfigFile()
        {
            SelectConfigFile<T>();
        }

        protected static T GetConfigFile()
        {
            return GetConfigFile<T>();
        }
        
        public static void SelectConfigFile<T>() where T : ScriptableObject
        {
            var asset = GetConfigFile<T>();

            if (asset != null)
            {
                UnityEditor.EditorUtility.FocusProjectWindow();

                UnityEditor.Selection.activeObject = asset;
            }
            else
            {
                Debug.LogErrorFormat("Not found a config file: {0}", typeof(T).FullName);
            }
        }

        public static T GetConfigFile<T>() where T : ScriptableObject
        {
            string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + typeof(T).FullName, null);

            foreach (string guid in guids)
            {
                try
                {
                    string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
                    var asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);

                    return asset;
                }
                catch (Exception exception)
                {
                    Debug.LogException(exception);
                }
            }

            return null;
        }

        public static void CreateAsset()
        {
            if (Instance != null)
            {
                return;
            }

            var asset = CreateInstance<T>();
            string pathFoler = System.IO.Path.Combine("Assets/Resources/", FILE_PATH);
            if (!System.IO.Directory.Exists(pathFoler))
                System.IO.Directory.CreateDirectory(pathFoler);
            string path = System.IO.Path.Combine(pathFoler, PREFAB_NAME + ".asset");
            
            string assetPathAndName =
                AssetDatabase.GenerateUniqueAssetPath(path);

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        
#endif // UNITY_EDITOR
    }
}
