﻿using System;

namespace OAZ1S.BuildPipeline
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class BuildPipelineCommandAttribute : Attribute
    {
    }
}
