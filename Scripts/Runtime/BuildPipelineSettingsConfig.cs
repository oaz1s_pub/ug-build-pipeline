using UnityEngine;

#if UNITY_EDITOR

//using Sirenix.OdinInspector;
using UnityEditor;
#endif // UNITY_EDITOR

namespace OAZ1S.BuildPipeline
{
	public class BuildPipelineSettingsConfig : IConfig<BuildPipelineSettingsConfig>
	{
		static BuildPipelineSettingsConfig()
		{
			PREFAB_NAME = "BuildPipelineSettingsConfig";
			FILE_PATH = "BuildPipeline/";
		}
		[SerializeField] protected string _buildVersion;
		[SerializeField] protected int _versionCode;
		[SerializeField] protected bool _debugBuild;
		[SerializeField] protected bool _deepProfiling;
		[SerializeField] protected bool _devUI;

		public static string SafeFullVersion => Instance != null ? $"{Instance.BuildVersion}({Instance.VersionCode})" : Application.version;
		
		public static string SafeBuildVersion => Instance != null ? Instance.BuildVersion : Application.version;
		
		public string BuildVersion
		{
			get => _buildVersion;
#if UNITY_EDITOR
			set => _buildVersion = value;
#endif // UNITY_EDITOR
		}
		
		public static int SafeVersionCode => Instance != null ? Instance.VersionCode : -1;

		public int VersionCode
		{
			get => _versionCode;
#if UNITY_EDITOR
			set => _versionCode = value;
#endif // UNITY_EDITOR
		}
		
		public static bool SafeDebugBuild => Instance != null && Instance.DebugBuild;

		public bool DebugBuild
		{
			get => _debugBuild;
#if UNITY_EDITOR
			set => _debugBuild = value;
#endif // UNITY_EDITOR
		}

		public static bool SafeDeepProfiling => Instance != null && Instance.DeepProfiling;

		public bool DeepProfiling
		{
			get => _deepProfiling;
#if UNITY_EDITOR
			set => _deepProfiling = value;
#endif // UNITY_EDITOR
		}

		public bool SafeDevUI => Instance != null && Instance.DevUI;

		public bool DevUI
		{
			get => _devUI;
#if UNITY_EDITOR
			set => _devUI = value;
#endif // UNITY_EDITOR
		}

#if UNITY_EDITOR
		
		//[Button, ButtonGroup]
		[ContextMenu("Save")]
		public void Save()
		{
			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssets();

			AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
		}

		public static void SetSettings(string version, int buildNumber, bool devUI, bool debugBuild, bool deepProfiling)
		{
			var asset = Instance;
			if (asset == null)
			{
				BuildPipelineSettingsConfig.CreateAsset();
				asset = BuildPipelineSettingsConfig.Instance;
			}

			asset.BuildVersion = version;
			asset.VersionCode = buildNumber;
            
			asset.DevUI = devUI;
			asset.DebugBuild = debugBuild;
			asset.DeepProfiling = deepProfiling;
            
			asset.Save();
		}
		
#endif // UNITY_EDITOR
	}
}
