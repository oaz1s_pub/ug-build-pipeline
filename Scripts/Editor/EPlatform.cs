using UnityEditor;

namespace OAZ1S.BuildPipeline.Editor
{
    public enum EPlatform
    {
        None,
        IOS,
        Android
    }
    
    public static class EPlatformExtension
    {
        public static BuildTarget GetTargetByPlatform(this EPlatform platform)
        {
            switch (platform)
            {
                case EPlatform.IOS:
                    return BuildTarget.iOS;
                case EPlatform.Android:
                    return BuildTarget.Android;
            }
            return BuildTarget.NoTarget;
        }
        
        public static EPlatform GetPlatformByTarget(this BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.iOS:
                    return EPlatform.IOS;
                case BuildTarget.Android:
                    return EPlatform.Android;
            }
            return EPlatform.None;
        }
    }
}