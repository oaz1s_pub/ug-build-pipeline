using UnityEditor;
using UnityEngine;

namespace OAZ1S.BuildPipeline.Editor
{
    public static class BuildPipelineButtons
    {
        [MenuItem("Build/Build iOS", priority = 501)]
		public static void CI_BuildiOS()
		{
			Debug.Log("BuildPipeline -> CI_BuildiOS -> START 1!");
			
			BuildPipeline.RunPreBuildCommandsAndBuild(BuildTarget.iOS);
		}

		[MenuItem("Build/Build Android", priority = 502)]
		public static void CI_BuildAndroid()
		{
			BuildPipeline.RunPreBuildCommandsAndBuild(BuildTarget.Android);
		}
		
		[MenuItem("Build/Build apk dev inc", priority = 503)]
		public static void CI_BuildAndroidApkDevIncrement()
		{
			BuildPipelineEditorConfig.Instance.BuildNumber++;
			BuildPipelineEditorConfig.Instance.DevUI = true;
			//CustomBuildConfigs.Instance.DebugBuild = true;
			BuildPipelineEditorConfig.Instance.DeepProfiling = false;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.GoogleBundle = false;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.Architecture = AndroidArchitecture.ARMv7;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.ScriptingImplementation =
				ScriptingImplementation.Mono2x;
			BuildPlatformsEditorConfig.Instance.Apply();
			BuildPipelineEditorConfig.Instance.Apply();
			BuildPipeline.RunPreBuildCommandsAndBuild(BuildTarget.Android);
		}
		
		[MenuItem("Build/Build apk inc", priority = 504)]
		public static void CI_BuildAndroidApkIncrement()
		{
			BuildPipelineEditorConfig.Instance.BuildNumber++;
			BuildPipelineEditorConfig.Instance.DevUI = false;
			BuildPipelineEditorConfig.Instance.DebugBuild = false;
			BuildPipelineEditorConfig.Instance.DeepProfiling = false;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.GoogleBundle = false;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.Architecture = AndroidArchitecture.ARMv7;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.ScriptingImplementation =
				ScriptingImplementation.Mono2x;
			BuildPlatformsEditorConfig.Instance.Apply();
			BuildPipelineEditorConfig.Instance.Apply();
			BuildPipeline.RunPreBuildCommandsAndBuild(BuildTarget.Android);
		}
		
		[MenuItem("Build/Build abb ", priority = 505)]
		public static void CI_BuildAndroidAbb()
		{
			BuildPipelineEditorConfig.Instance.DevUI = false;
			BuildPipelineEditorConfig.Instance.DebugBuild = false;
			BuildPipelineEditorConfig.Instance.DeepProfiling = false;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.Architecture =
				AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.ScriptingImplementation =
				ScriptingImplementation.IL2CPP;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.GoogleBundle = true;
			BuildPlatformsEditorConfig.Instance.AndroidSettings.Symbols = true;
			BuildPlatformsEditorConfig.Instance.Apply();
			BuildPipelineEditorConfig.Instance.Apply();
			BuildPipeline.RunPreBuildCommandsAndBuild(BuildTarget.Android);
		}
    }
}