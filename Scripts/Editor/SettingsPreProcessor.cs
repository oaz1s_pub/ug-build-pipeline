using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace OAZ1S.BuildPipeline.Editor
{
    public class SettingsPreProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get { return 0; } }
        public void OnPreprocessBuild(BuildReport report)
        {
            var cbc = BuildPipelineEditorConfig.Instance;
            BuildPipelineSettingsConfig.SetSettings(cbc.Version, cbc.BuildNumber, cbc.DevUI, cbc.DebugBuild, cbc.DeepProfiling);
        }
    }
}
