using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace OAZ1S.BuildPipeline.Editor
{ 
	class BuildPipelineWindow : OdinEditorWindow
	{
		[SerializeField, EnumToggleButtons, BoxGroup("Platform")]
		private EPlatform _selectedTarget = EPlatform.None;
		private EPlatform _activeTarget = EPlatform.None;

		private bool _isSwitchingPlatform = false;
		private DateTime compileStartTime;
		[SerializeField, Title("Build settings"), FoldoutGroup("Build settings"), InlineEditor(InlineEditorObjectFieldModes.CompletelyHidden)]
		private BuildPipelineEditorConfig buildPipelineEditorConfig;
		[SerializeField, Title("Platforms settings"), FoldoutGroup("Platforms settings"), InlineEditor(InlineEditorObjectFieldModes.CompletelyHidden)]
		private BuildPlatformsEditorConfig buildPlatformsEditorConfig;

		[MenuItem("File/BuildPipeline/Build Settings... %#w")]
		public static void BuildGameSettings()
		{
			var buildPipelineWindow = GetWindow<BuildPipelineWindow>();
			buildPipelineWindow.name = "Build Pipeline";
			buildPipelineWindow.titleContent = new GUIContent(buildPipelineWindow.name);

			Vector2 initialSize = new Vector2(350f, 660f);
			buildPipelineWindow.minSize = initialSize;
			buildPipelineWindow.position = new Rect((Screen.currentResolution.width - initialSize.x) * 0.5f,
				(Screen.currentResolution.height - initialSize.y) * 0.5f, initialSize.x, initialSize.y);

			buildPipelineWindow.Show();
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			UpdateConfigs();
			_selectedTarget = _activeTarget;
		}

		private void OnFocus() => UpdateConfigs();

		private void UpdateConfigs()
		{
			DetermineCurrentPlatform();
			if(BuildPipelineEditorConfig.Instance == null)
				BuildPipelineEditorConfig.CreateAsset();
			buildPipelineEditorConfig = BuildPipelineEditorConfig.Instance;
			buildPipelineEditorConfig.Init();
			if(BuildPlatformsEditorConfig.Instance == null)
				BuildPlatformsEditorConfig.CreateAsset();
			buildPlatformsEditorConfig = BuildPlatformsEditorConfig.Instance;
			if(BuildPipelineSettingsConfig.Instance == null)
				BuildPipelineSettingsConfig.CreateAsset();
		}

		private void Update()
		{
			if (_isSwitchingPlatform)
			{
				if (_selectedTarget != _activeTarget)
				{
					EditorUserBuildSettings.SwitchActiveBuildTarget(
						BuildPipeline.GetBuildTargetGroup(_selectedTarget.GetTargetByPlatform()),
						_selectedTarget.GetTargetByPlatform());

					_activeTarget = _selectedTarget;

					compileStartTime = DateTime.Now.ToUniversalTime();
				}
				else
				{
					if (!EditorApplication.isCompiling)
					{
						EditorUtility.ClearProgressBar();

						_isSwitchingPlatform = false;
					}
					else
					{
						double compileTime = (DateTime.Now.ToUniversalTime() - compileStartTime).TotalSeconds;
						
						EditorUtility.DisplayProgressBar("Recompiling scripts...", "Compile time: " + compileTime,
							(float) compileTime / 7f);
					}
				}
			}
		}

		[Button, PropertySpace(SpaceBefore = 5, SpaceAfter = 5)]
		private void RunButtonCommands()
		{
			BuildPipeline.RunButtonCommands();
		}
			
		[Button, PropertySpace(SpaceBefore = 5, SpaceAfter = 5)]
		private void PlayerSettings()
		{
			BuildPipeline.ShowPlayerSettings();
		}

		[DisableIf("_isSwitchingPlatform"), Button(ButtonSizes.Large), GUIColor(0.2f,1f, 0.2f),PropertySpace(SpaceBefore = 10)]
		private void Build()
		{
			EditorApplication.delayCall += () =>
			{
				BuildPipeline.RunPreBuildCommandsAndBuild(_activeTarget.GetTargetByPlatform());
			};
		}


#region Platform
		[Button, BoxGroup("Platform"), ShowIf("@this._selectedTarget != this._activeTarget && this._selectedTarget != EPlatform.None")]
		private void SwitchPlatform()
		{
			DetermineCurrentPlatform();
			_isSwitchingPlatform = true;
		}

		private void DetermineCurrentPlatform()
		{
			_activeTarget = EditorUserBuildSettings.activeBuildTarget.GetPlatformByTarget();
		}

#endregion
		
		
	}
}