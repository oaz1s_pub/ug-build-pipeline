using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor.Build.Reporting;

namespace OAZ1S.BuildPipeline.Editor
{
	public class BuildPipeline
	{
		public static Dictionary<BuildTarget, Texture> platformsIcons =
			new Dictionary<BuildTarget, Texture>
			{
				{BuildTarget.iOS, EditorGUIUtility.FindTexture("BuildSettings.iPhone.Small")},
				{BuildTarget.Android, EditorGUIUtility.FindTexture("BuildSettings.Android.Small")},
			};
		
		private static Dictionary<BuildTarget, BuildTargetGroup> platformsGroupTypes =
			new Dictionary<BuildTarget, BuildTargetGroup>
			{
				{BuildTarget.iOS, BuildTargetGroup.iOS},
				{BuildTarget.Android, BuildTargetGroup.Android},
			};

		public static BuildTargetGroup GetBuildTargetGroup(BuildTarget platform)
		{
			if (platformsGroupTypes.ContainsKey(platform))
				return platformsGroupTypes[platform];
			return BuildTargetGroup.Unknown;
		}

		[MenuItem("File/BuildPipeline/Player Settings...")]
		public static void ShowPlayerSettings()
		{
#if UNITY_2018_1_OR_NEWER

			SettingsService.OpenProjectSettings("Project/Player");
#else
			EditorApplication.ExecuteMenuItem("Edit/Project Settings/Player");
#endif

		}

		private static List<MethodInfo> GetCommands<T>() where T : Attribute
		{
			// Returns all currenlty loaded assemblies
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();

			// returns all types defined in this assemblies
			var types = assemblies.SelectMany(x => x.GetTypes());

			// only yields classes
			var classes = types.Where(x => x.IsClass);

			// returns all methods defined in those classes
			var methods = classes.SelectMany(x => x.GetMethods(BindingFlags.Static | BindingFlags.NonPublic));

			// returns only methods that have the InvokeAttribute
			var invokers = methods.Where(x => x.GetCustomAttributes(typeof(T), false).Length != 0).ToList();

			return invokers;
		}

		[MenuItem("Build/Run Button Commands", priority = 101)]
		public static void RunButtonCommands(Action callback = null)
		{
			var args = BuildPipelineEditorConfig.Instance.ButtonCommands;

			InvokeAttributions(args, OnComplete);

			if (!Application.isBatchMode)
			{
				EditorUtility.DisplayProgressBar("Commands", "Please Wait", 0.5f);
			}

			void OnComplete()
			{
				if (!Application.isBatchMode)
				{
					EditorApplication.delayCall += EditorUtility.ClearProgressBar;
				}

				Debug.Log("BuildPipeline -> ButtonCommands -> OnComplete");
			}
		}
		
		[MenuItem("Build/Run Pre Build Commands", priority = 102)]
		public static void RunPreBuildCommands(Action callback = null)
		{
			var args = BuildPipelineEditorConfig.Instance.PreBuildCommands;

			InvokeAttributions(args, OnComplete);

			if (!Application.isBatchMode)
			{
				EditorUtility.DisplayProgressBar("Commands", "Please Wait", 0.5f);
			}

			void OnComplete()
			{
				if (!Application.isBatchMode)
				{
					EditorApplication.delayCall += () =>
					{
						EditorUtility.ClearProgressBar();
					};
				}
				callback?.Invoke();
				Debug.Log("BuildPipeline -> RunPreBuildCommands -> OnComplete");
			}
		}

		private static void InvokeAttributions(string[] args, Action callback)
		{
			bool completed = false;
			
			var invokers = GetCommands<BuildPipelineCommandAttribute>();
			int counter = invokers.Count;
			
			for (int i = 0; i < invokers.Count; i++)
			{
				var method = invokers[i];

				string name = method.Name;
				var type = method.DeclaringType;

				if (type == null)
				{
					continue;
				}
				
				Debug.LogFormat("BuildPipeline -> InvokeAsyncAttributions -> type = {0} method = {1}", type.FullName, name);
				
				Action<bool> tempCallback = OnInvoked;

				try
				{
					bool async = false;
					object[] parameters = null;
					var parametersInfo = method.GetParameters();

					switch (parametersInfo.Length)
					{
						case 0:
							break;
						case 1:
							parameters = new object[] {args};
							break;
						case 2:
							parameters = new object[] {args, tempCallback};
							async = true;
							break;
					}
					
					method.Invoke(null, parameters); // invoke the method
					
					if (!async)
					{
						tempCallback?.Invoke(true);
					}
				}
				catch (Exception exception)
				{
					counter--;
					
					Debug.LogException(exception);
				}

				void OnInvoked(bool result)
				{
					Debug.LogFormat("BuildPipeline -> InvokeAsyncAttributions -> Result -> type = {0} method = {1} result = {2}", 
						type.FullName, name, result);
					
					counter--;

					CheckCommand();
				}
			}

			if (!completed)
			{
				CheckCommand();
			}

			void CheckCommand()
			{
				if (completed)
				{
					return;
				}
				
				Debug.LogFormat("BuildPipeline -> InvokeAsyncAttributions -> counter = {0}", counter);
				
				if (counter == 0)
				{
					completed = true;
					
					callback?.Invoke();
				}
			}
		}

		private static void CheckBuildConfigs()
		{
			if (BuildPipelineEditorConfig.Instance == null)
			{
				BuildPipelineEditorConfig.CreateAsset();
			}
			if (BuildPlatformsEditorConfig.Instance == null)
			{
				BuildPlatformsEditorConfig.CreateAsset();
			}
		}

		public static void Build_Command()
		{
			CheckBuildConfigs();
			
			var platform = EditorUserBuildSettings.activeBuildTarget;
			
			Debug.LogFormat("BuildPipeline -> CI_Build_Command platform = {0}", platform.ToString());

			var buildResult = BuildResult.Unknown;
			
			var args = System.Environment.GetCommandLineArgs();

			string outputPath = string.Empty;

			bool justUpdate = false;
			
			Debug.LogFormat("BuildPipeline -> CI_Build_Command: '{0}'", string.Join(",", args));

			for (int i = 0; i < args.Length; i++)
			{
				switch (args[i])
				{
					case "-outputPath":
						if (i < args.Length - 1)
						{
							outputPath = args[i + 1];
						}
						break;
					case "-justUpdate":
						justUpdate = true;
						break;
				}
			}
			
			BuildPipelineEditorConfig.Instance.ParseArgs(platform, args);
			BuildPipelineEditorConfig.Instance.Save();
			
			BuildPlatformsEditorConfig.Instance.ParseArgs(platform, args);
			BuildPlatformsEditorConfig.Instance.Save();

			Debug.LogFormat("BuildPipeline -> CI_Build_Command -> outputPath = {0}", outputPath);

			InvokeAttributions(args, OnInvokersComplete);

			void OnInvokersComplete()
			{
				Debug.Log("BuildPipeline -> CI_Build_Command -> OnInvokersComplete");
				
				Debug.LogFormat("BuildPipeline -> CI_Build_Command -> StartBuild -> {0}", outputPath);

				if (!justUpdate)
				{
					Debug.LogFormat("BuildPipeline -> CI_Build_Command -> platform = {0}", platform);

					if (!string.IsNullOrEmpty(outputPath))
					{
						buildResult = BuildGame(outputPath, platform);
					}
					else
					{
						buildResult = BuildGame(platform);
					}
				}

				EditorApplication.Exit((buildResult == BuildResult.Failed) ? 1 : 0);
			}
		}

		public static void RunPreBuildCommandsAndBuild(BuildTarget platform)
		{
			EditorApplication.delayCall += () =>
			{
				BuildPipeline.RunPreBuildCommands(OnCommandsComplete);
			};

			void OnCommandsComplete()
			{
				BuildGame(platform);
			}
		}

		private static BuildResult BuildGame(BuildTarget platform)
		{
			ApplyConfigs();
			
			string filename;
			string outputPath = Application.dataPath;
			string devPostfix = "";
			
#if DEV_UI
			devPostfix = "_d";
#endif
			string saveFileTitle = "Select build path";
			
			switch (platform)
			{
				case BuildTarget.StandaloneOSX:
					outputPath = EditorUtility.SaveFilePanel("Select build path",
						EditorUserBuildSettings.GetBuildLocation(EditorUserBuildSettings.activeBuildTarget), "", "");
					break;
				case BuildTarget.Android:
					var appName = PlayerSettings.productName.Replace("/", "").Replace(":", "");
					filename =
						$"{appName}_{BuildPipelineEditorConfig.Instance.Version}_v{BuildPipelineEditorConfig.Instance.BuildNumber}{devPostfix}";
					
					if(!EditorUserBuildSettings.buildAppBundle)
					{
						filename = $"{filename}.apk";
					}
					else
					{
						filename = $"{filename}.aab";
					}

					outputPath = Path.Combine(BuildPlatformsEditorConfig.Instance.AndroidSettings.BuildPath, filename);
					if (string.IsNullOrEmpty(outputPath))
					{
						outputPath = EditorUtility.SaveFilePanel(saveFileTitle, outputPath, filename, "");
					}
					break;
				case BuildTarget.iOS:
					filename =
						$"{PlayerSettings.applicationIdentifier}_{BuildPipelineEditorConfig.Instance.Version}_b{BuildPipelineEditorConfig.Instance.BuildNumber}{devPostfix}/";
					
					outputPath = Path.Combine(BuildPlatformsEditorConfig.Instance.IosSettings.BuildPath, filename);
					if (string.IsNullOrEmpty(outputPath))
					{
						outputPath = EditorUtility.SaveFilePanel(saveFileTitle, outputPath, filename, "");
					}
					break;
				default:
					outputPath = EditorUtility.SaveFolderPanel(saveFileTitle,
						EditorUserBuildSettings.GetBuildLocation(EditorUserBuildSettings.activeBuildTarget), "");
					break;
			}

			if (string.IsNullOrEmpty(outputPath))
			{
				EditorUserBuildSettings.SetBuildLocation(EditorUserBuildSettings.activeBuildTarget, outputPath);
			}

			return BuildGame(outputPath, platform);
		}

		private static void ApplyConfigs()
		{
			CheckBuildConfigs();
			
			BuildPipelineEditorConfig.Instance.Apply();
			BuildPlatformsEditorConfig.Instance.Apply();
			
			SetBuildVersion(BuildPipelineEditorConfig.Instance.Version);
			SetVersionCode(BuildPipelineEditorConfig.Instance.Version, BuildPipelineEditorConfig.Instance.BuildNumber);
			SetBuildNumber(BuildPipelineEditorConfig.Instance.Version, BuildPipelineEditorConfig.Instance.BuildNumber);
		}
		
		private static BuildResult BuildGame(string outputPath, BuildTarget platform)
		{
			if (string.IsNullOrEmpty(outputPath))
			{
				return BuildResult.Failed;
			}

			ApplyConfigs();

			Debug.Log("BuildPipeline -> BuildGame -> Start custom build pipeline...");

			var result = BuildGame(platform, outputPath);

			return result;
		}

		private static BuildResult BuildGame(BuildTarget platform, string outputPath)
		{
			List<string> levels = new List<string>();

			if (SystemInfo.graphicsDeviceID != 0)
			{
				EditorUtility.DisplayProgressBar("Adding scenes", null, 0);
			}

			int index = 0;
			foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
			{
				if (SystemInfo.graphicsDeviceID != 0)
				{
					EditorUtility.DisplayProgressBar("Adding scenes", scene.path,
						((float) index / (float) EditorBuildSettings.scenes.Length));
				}
				Debug.LogFormat("BuildPipeline -> BuildGame -> Add scene = {0}", scene.path);
				levels.Add(scene.path);
				index++;
			}

#if UNITY_IOS
			PlayerSettings.iOS.hideHomeButton = true;
			PlayerSettings.iOS.deferSystemGesturesMode = UnityEngine.iOS.SystemGestureDeferMode.All;
#endif

			var options = BuildOptions.None;
			
			if (BuildPipelineEditorConfig.Instance.DebugBuild)
			{
				options |= BuildOptions.Development | BuildOptions.AllowDebugging;
			}
			
			if (BuildPipelineEditorConfig.Instance.DeepProfiling)
			{
				options |= BuildOptions.EnableDeepProfilingSupport;
			}
			
			BuildReport report = UnityEditor.BuildPipeline.BuildPlayer(
				levels.ToArray(), outputPath,
				EditorUserBuildSettings.activeBuildTarget, options
			);
			
			EditorUtility.ClearProgressBar();

			AssetDatabase.Refresh();

			BuildResult result = report.summary.result;

			Debug.LogFormat("BuildPipeline -> BuildGame -> result = {0}", result);
			
			// if(Application.isBatchMode)
			// {
			// 	EditorApplication.Exit(code);
			// }
			
			return result;
		}

		[MenuItem("File/BuildPipeline/Empty Cache")]
		static void EmptyCache()
		{
			Caching.ClearCache();
		}
		
		public static void SetBuildVersion(string version)
		{
			Debug.LogFormat("BuildPipeline -> SetBuildVersion -> version = {0}", version);

			PlayerSettings.bundleVersion = version;
		}

		public static void SetBuildNumber(string appVersion, int buildNumber)
		{
			Debug.LogFormat("BuildPipeline -> SetBuildNumber -> buildNumber = {0}", buildNumber);

			var version = Version.Parse(appVersion);
			string value = $"{version.Major:D}{version.Minor:D2}{version.Build:D2}{buildNumber:D2}";

			PlayerSettings.iOS.buildNumber = value;
		}

		public static void SetVersionCode(string appVersion, int versionCode)
		{
			Debug.LogFormat("BuildPipeline -> SetVersionCode -> versionCode = {0}", versionCode);
			
			var version = Version.Parse(appVersion);
			int value = versionCode + version.Build * 100 + version.Minor * 10000 + version.Major * 1000000;

			PlayerSettings.Android.bundleVersionCode = value;
		}
	}
}
