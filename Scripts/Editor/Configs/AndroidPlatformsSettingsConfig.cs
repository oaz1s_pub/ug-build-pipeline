﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

namespace OAZ1S.BuildPipeline.Editor
{
	[Serializable]
	public class AndroidPlatformsSettingsConfig : IPlatformsSettingsConfig
	{
		[SerializeField] public string KeystorePath;
		[SerializeField] public string KeystorePassword;
		[SerializeField] public string KeystoreAlias;
		[SerializeField] public string KeystoreAliasPassword;

		[SerializeField] public AndroidArchitecture Architecture = AndroidArchitecture.ARMv7;

		[SerializeField] public bool GoogleBundle = false;
		
		[SerializeField] public bool Symbols = false;

		[SerializeField] public ScriptingImplementation ScriptingImplementation = ScriptingImplementation.Mono2x;

		public override void ParseArgs(string[] args)
		{
			bool googleBundle = false;
			bool useIl2cpp = false;

			for (int i = 0; i < args.Length; i++)
			{
				switch (args[i])
				{
					case "-identifier":
						if (i < args.Length - 1)
						{
							BundleId = args[i + 1];
						}

						break;
					case "-googleBundle":
						googleBundle = true;
						break;
					case "-useIl2cpp":
						useIl2cpp = true;
						break;
					case "-androidArchitecture":
						if (i < args.Length - 1 && !string.IsNullOrEmpty(args[i + 1]))
						{
							if (Enum.TryParse(args[i + 1], out AndroidArchitecture architecture))
							{
								Architecture = architecture;
							}
						}
						break;
				}
			}

			GoogleBundle = googleBundle;
			ScriptingImplementation =
				useIl2cpp ? ScriptingImplementation.IL2CPP : ScriptingImplementation.Mono2x;
		}
		
#if UNITY_EDITOR

		public override void Load(bool force)
		{
			BundleId = PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.Android);

			if (force)
			{
				KeystorePath = PlayerSettings.Android.keystoreName;
				KeystorePassword = PlayerSettings.Android.keystorePass;
				KeystoreAlias = PlayerSettings.Android.keyaliasName;
				KeystoreAliasPassword = PlayerSettings.Android.keyaliasPass;

				GoogleBundle = EditorUserBuildSettings.buildAppBundle;
				ScriptingImplementation = PlayerSettings.GetScriptingBackend(BuildTargetGroup.Android);
				Architecture = PlayerSettings.Android.targetArchitectures;
				Symbols = EditorUserBuildSettings.androidCreateSymbols == AndroidCreateSymbols.Public;
			}
		}

		public override void Apply()
		{
			if (!string.IsNullOrEmpty(BundleId))
			{
				PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, BundleId);
			}

			PlayerSettings.Android.useCustomKeystore = !string.IsNullOrEmpty(KeystorePath);

			PlayerSettings.Android.keystoreName = KeystorePath;
			PlayerSettings.Android.keystorePass = KeystorePassword;
			PlayerSettings.Android.keyaliasName = KeystoreAlias;
			PlayerSettings.Android.keyaliasPass = KeystoreAliasPassword;

			EditorUserBuildSettings.exportAsGoogleAndroidProject = false;
			
			EditorUserBuildSettings.buildAppBundle = GoogleBundle;
			PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation);
			PlayerSettings.Android.targetArchitectures = Architecture;
			EditorUserBuildSettings.androidCreateSymbols = Symbols ? AndroidCreateSymbols.Public : AndroidCreateSymbols.Disabled;
		}

		private bool _showPosition;
		private const string status = "Android Settings";

		public override void DrawGUI()
		{
			GUILayout.BeginVertical(EditorStyles.helpBox);

			var content = new GUIContent(status, BuildPipeline.platformsIcons[BuildTarget.Android]);
			_showPosition = EditorGUILayout.Foldout(_showPosition, content);

			if (_showPosition)
			{
				BundleId = EditorGUILayout.TextField("Bundle Identifier", BundleId);
				BuildPath = EditorGUILayout.TextField("Build Path", BuildPath);
				
				GUILayout.Space(10);

				GUI.changed = false;
				GUILayout.Toggle(ScriptingImplementation == ScriptingImplementation.IL2CPP,
					"Use IL2CPP");
				if (GUI.changed)
				{
					if (ScriptingImplementation == ScriptingImplementation.IL2CPP)
					{
						ScriptingImplementation = ScriptingImplementation.Mono2x;
					}
					else
					{
						ScriptingImplementation = ScriptingImplementation.IL2CPP;
					}
				}

				Architecture = (AndroidArchitecture) EditorGUILayout.EnumFlagsField("Architecture", Architecture);
				
				GUILayout.Space(10);

				GoogleBundle = GUILayout.Toggle(GoogleBundle, "Google Bundle");

				GUILayout.Space(10);

				GUILayout.Label("Keystore", EditorStyles.boldLabel);

				KeystorePath = EditorGUILayout.TextField("keystoreName", KeystorePath);
				KeystorePassword = EditorGUILayout.TextField("keystorePass", KeystorePassword);
				KeystoreAlias = EditorGUILayout.TextField("keyaliasName", KeystoreAlias);
				KeystoreAliasPassword = EditorGUILayout.TextField("keyaliasPass", KeystoreAliasPassword);

				GUILayout.Space(10);

				GUILayout.BeginHorizontal();

				if (GUILayout.Button("Load"))
				{
					Load(true);
				}

				if (GUILayout.Button("Apply"))
				{
					Apply();
				}

				GUILayout.EndHorizontal();
			}

			GUILayout.EndVertical();
		}
#endif // UNITY_EDITOR
	}
}