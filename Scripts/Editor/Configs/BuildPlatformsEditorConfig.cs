using OAZ1S.BuildPipeline;
using Sirenix.OdinInspector;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

namespace OAZ1S.BuildPipeline.Editor
{
    [CreateAssetMenu(fileName = "BuildPlatformsEditorConfig", menuName = "BuildPipeline/", order = 2)]
    public class BuildPlatformsEditorConfig : IConfig<BuildPlatformsEditorConfig>
    {
	    static BuildPlatformsEditorConfig()
        {
	        PREFAB_NAME = "BuildPlatformsEditorConfig";
	        FILE_PATH = "BuildPipeline/Editor/";
        }

        [SerializeField, TabGroup("Android"), InlineProperty, HideLabel]
        public AndroidPlatformsSettingsConfig AndroidSettings;
        [SerializeField, TabGroup("iOS"), InlineProperty, HideLabel]
        public IosPlatformsSettingsConfig IosSettings;

        public void ParseArgs(BuildTarget platform, string[] args)
        {
	        AndroidSettings.ParseArgs(args);
	        IosSettings.ParseArgs(args);
        }
        
#if UNITY_EDITOR

        public void DrawGUI()
        {
	        AndroidSettings.DrawGUI();
	        IosSettings.DrawGUI();
	        
	        GUILayout.BeginHorizontal(EditorStyles.helpBox);

	        if (GUILayout.Button("Load"))
	        {
		        Load();
	        }

	        if (GUILayout.Button("Apply"))
	        {
		        Apply();
	        }

	        if (GUILayout.Button("Save"))
	        {
		        Save();
	        }
	        
	        GUILayout.EndHorizontal();
        }

        [Button, ButtonGroup]
        public void Save()
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();

            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        [Button, ButtonGroup]
        public void Load()
        {
            AndroidSettings.Load(false);
            IosSettings.Load(false);

            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();

            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        [Button, ButtonGroup]
        public void Apply()
        {
            AndroidSettings.Apply();
            IosSettings.Apply();
            
            Save();
        }
        
#endif // UNITY_EDITOR
    }
}
