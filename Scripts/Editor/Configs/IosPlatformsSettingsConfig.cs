﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

namespace OAZ1S.BuildPipeline.Editor
{
	[Serializable]
	public class IosPlatformsSettingsConfig : IPlatformsSettingsConfig
	{
		[SerializeField] public string TeamId;

		[SerializeField] public bool AutomaticSigning = true;
		
		public override void ParseArgs(string[] args)
		{
			for (int i = 0; i < args.Length; i++)
			{
				switch (args[i])
				{
					case "-identifier":
						if (i < args.Length - 1)
						{
							BundleId = args[i + 1];
						}

						break;
				}
			}
		}
		
#if UNITY_EDITOR

		public override void Load(bool force)
		{
			BundleId = PlayerSettings.GetApplicationIdentifier(BuildTargetGroup.iOS);

			if (force)
			{
				TeamId = PlayerSettings.iOS.appleDeveloperTeamID;
				AutomaticSigning = PlayerSettings.iOS.appleEnableAutomaticSigning;
			}
		}

		public override void Apply()
		{
			if (!string.IsNullOrEmpty(BundleId))
			{
				PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, BundleId);
			}

			PlayerSettings.iOS.appleDeveloperTeamID = TeamId;
			PlayerSettings.iOS.appleEnableAutomaticSigning = AutomaticSigning;
		}

		private bool _showPosition;
		private const string status = "iOS Settings";

		public override void DrawGUI()
		{
			GUILayout.BeginVertical(EditorStyles.helpBox);

			var content = new GUIContent(status, BuildPipeline.platformsIcons[BuildTarget.iOS]);
			_showPosition = EditorGUILayout.Foldout(_showPosition, content);

			if (_showPosition)
			{
				BundleId = EditorGUILayout.TextField("Bundle Identifier", BundleId);
				BuildPath = EditorGUILayout.TextField("Build Path", BuildPath);

				GUILayout.Space(10);

				TeamId = EditorGUILayout.TextField("TeamId", TeamId);
				AutomaticSigning = GUILayout.Toggle(AutomaticSigning, "Automatic Signing");

				GUILayout.Space(10);

				GUILayout.BeginHorizontal();

				if (GUILayout.Button("Load"))
				{
					Load(true);
				}

				if (GUILayout.Button("Apply"))
				{
					Apply();
				}

				GUILayout.EndHorizontal();
			}

			GUILayout.EndVertical();
		}
#endif // UNITY_EDITOR
	}
}