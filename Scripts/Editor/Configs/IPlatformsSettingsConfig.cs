﻿using System;
using UnityEngine;

namespace OAZ1S.BuildPipeline.Editor
{
    [Serializable]
    public abstract class IPlatformsSettingsConfig
    {
        [SerializeField] public string BuildPath = "./Builds";
        [SerializeField] public string BundleId;

        public abstract void ParseArgs(string[] args);
        
        public abstract void Load(bool force);
        public abstract void Apply();

        public abstract void DrawGUI();
    }
}