﻿using Sirenix.OdinInspector;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR

namespace OAZ1S.BuildPipeline.Editor
{
    [CreateAssetMenu(fileName = "BuildPipelineEditorConfig", menuName = "BuildPipeline/", order = 1)]
    public class BuildPipelineEditorConfig : IConfig<BuildPipelineEditorConfig>
    {
        static BuildPipelineEditorConfig()
        {
            PREFAB_NAME = "BuildPipelineEditorConfig";
            FILE_PATH = "BuildPipeline/Editor/";
        }

        [ReadOnly, BoxGroup("Version")]
        public string Version = "";
        [SerializeField, BoxGroup("Version"), HorizontalGroup("Version/Values"), ValidateInput("ValidateVersion", "Should be [0..99]"), HideLabel]
        private int _majorVersion = 0;
        [SerializeField, BoxGroup("Version"), HorizontalGroup("Version/Values"), ValidateInput("ValidateVersion", "Should be [0..99]"), HideLabel]
        private int _minorVersion = 0;
        [SerializeField, BoxGroup("Version"), HorizontalGroup("Version/Values"), ValidateInput("ValidateVersion", "Should be [0..99]"), HideLabel]
        private int _buildVersion = 1;

        [SerializeField, HorizontalGroup("BuildNumber", 150), ValidateInput("ValidateVersion", "Should be [1..99]")] 
        public int BuildNumber = 1;
        
        [SerializeField] public bool DevUI;
        [SerializeField] public bool DebugBuild;
        [SerializeField] public bool DeepProfiling;
        
        [SerializeField] public string[] PreBuildCommands = {"-updateLocalization"};
        [SerializeField] public string[] ButtonCommands = {"-updateLocalization"};

#if UNITY_EDITOR
	    
	    public void Init()
	    {
		    UpdateVersion();
	    }

	    private void UpdateVersion()
	    {
		    Version = $"{_majorVersion}.{_minorVersion}.{_buildVersion}";
	    }

        public void ParseArgs(BuildTarget platform, string[] args)
        {
	        string buildVersion = string.Empty;
	        string buildNumber = string.Empty;

	        DevUI = false;
	        DebugBuild = false;
			DeepProfiling = false;
	        
	        for (int i = 0; i < args.Length; i++)
	        {
		        switch (args[i])
		        {
			        case "-buildVersion":
				        if (i < args.Length - 1)
				        {
					        buildVersion = args[i + 1];
				        }
				        break;
			        case "-buildNumber":
				        if (i < args.Length - 1)
				        {
					        buildNumber = args[i + 1];
				        }
				        break;
			        case "-debugBuild":
				        DebugBuild = true;
				        break;
			        case "-devUI":
				        DevUI = true;
				        break;
		        }
	        }

	        if (!string.IsNullOrEmpty(buildVersion))
	        {
		        Version = buildVersion;
	        }

	        if (!string.IsNullOrEmpty(buildNumber) && int.TryParse(buildNumber, out int value))
	        {
		        BuildNumber = value;
	        }
        }

        [Button, ButtonGroup]
        public void Save()
        {
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();

            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        [Button, ButtonGroup]
        public void Load()
        {
            Version = PlayerSettings.bundleVersion;
            
            LoadVersion();

            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();

            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }

        [Button, ButtonGroup]
        public void Apply()
        {
	        ApplyVersion(true);
	        
	        BuildPipelineSettingsConfig.SetSettings(Version, BuildNumber, DevUI, DebugBuild, DeepProfiling);
            
            Save();
        }

        public virtual void LoadVersion()
        {
	        Version = PlayerSettings.bundleVersion;
	        
	        BuildNumber = Mathf.Max(PlayerSettings.Android.bundleVersionCode) % 100;
        }

        public virtual void ApplyVersion(bool useCustomPipeline)
        {
	        if (useCustomPipeline)
	        {
		        BuildPipeline.SetBuildVersion(Version);
		        BuildPipeline.SetVersionCode(Version, BuildNumber);
		        BuildPipeline.SetBuildNumber(Version, BuildNumber);
	        }
	        else
	        {
				PlayerSettings.bundleVersion = Version;
	        
		        var version = System.Version.Parse(Version);
		        int versionCode = BuildNumber + version.Build * 100 + version.Minor * 10000 + version.Major * 1000000;
	        
		        PlayerSettings.Android.bundleVersionCode = versionCode;
		        PlayerSettings.iOS.buildNumber = versionCode.ToString();
	        }
        }

        [Button("+"),HorizontalGroup("BuildNumber",20)]
        private void IncreaseBuildNumber()
        {
	        if((BuildNumber + 1) < 100)
				BuildNumber++;
        }
        
        [Button("-"),HorizontalGroup("BuildNumber",20)]
        private void DecreaseBuildNumber()
        {
	        if((BuildNumber - 1) > 0)
				BuildNumber--;
        }

#region Validating

	    private bool ValidateVersion(int value)
	    {
		    if (value < 0 || value > 99)
			    return false;
		    Init();
		    return true;
	    }
	    
#endregion
        
#endif // UNITY_EDITOR
    }
}
