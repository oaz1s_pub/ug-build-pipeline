# UG Build pipeline

Unity games build pipeline

 * [Install](#install)
 * [Build shortcats](#build-shortcats)
 * [Invokers](#invokers)
 * [BuildVersion](#buildversion)

### Install

Available starting from Unity 2018.3.

Just add this line to the `Packages/manifest.json` file of your Unity Project:

```json
"dependencies": {
    "com.oaz1s.ug_build_pipeline": "https://gitlab.com/oaz1s_pub/ug-build-pipeline.git#main",
}
```

Or just add this line through Package Manager

![Package manage instruction](./Img/package_manager_add_git.png)
```
            https://gitlab.com/oaz1s_pub/ug-build-pipeline.git#main
```

### Build shortcats

![BuildShortcats](./Img/build_short.png)

Easy build buttons. "Inc" buttons increment build number

### BuildVersion

BuildPipelineSettingsConfig.SafeFullVersion - return version by settings(kind of "0.1.0(2)") or Application.version if no config instance
BuildPipelineSettingsConfig.SafeBuildVersion - return version by settings(kind of "0.1.0") or Application.version if no config instance

Or use Instance(auto created) members

### Invokers

In build pipeline window it is possible to set up invokers commands.

![Invokers1](./Img/invokers1.png)

They are used for:
 - button in Build Pipeline window "Run All Invokers"
 - commands for building game from command line 
 
 Just add attribute to method you want to call as invoker
```c#
BuildPipelineCommandAttribute
```

There is three options for method signature:
- ```CheckBuildCommand() ```Without parameters. So this method will be called at any build by command line.
- ```CheckBuildCommand(string[] args) ``` It will pass all arguments to the method. So it is possible to check some arguments to perform the action.
- ```CheckBuildCommand(string[] args, Action<bool> callback) ``` Arguments as before and callback if it async operation that need time.

